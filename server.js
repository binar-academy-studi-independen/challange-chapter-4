const createError = require("http-errors");
const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const { PORT = 3000 } = process.env;

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const home = require("./routes/index");
const userGame = require("./routes/user");
const history = require("./routes/history");
app.use("/api/", home);
app.use("/api/", userGame);
app.use("/api/", history);

app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // render the error page
  res.status(err.status || 500);
  res.json({
    message: err.message,
  });
});

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
